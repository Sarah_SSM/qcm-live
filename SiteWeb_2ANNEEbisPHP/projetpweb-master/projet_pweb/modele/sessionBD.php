<?php


/** NOTE Gestion globale du test  *****************/

function afficher_test()
{
	require("modele/connect.php");
	$sql = "SELECT * FROM `test`";
	$tab_test = array();

	try {
		$commande = $pdo->prepare($sql);
		$bool = $commande->execute();
		if ($bool) {
			$tab_test = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			// var_dump($tab_test);
			// die('Arrête toi ici');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_test;
}

function activer_test($id)
{
	require("modele/connect.php");
	$sql = "UPDATE `test` SET bActif = 1 WHERE id_test =:id";

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id', $id);
		$bool = $commande->execute();
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function desactiver_test($id)
{
	require("modele/connect.php");
	$sql = "UPDATE `test` SET bActif = 0 WHERE id_test =:id;
		UPDATE `qcm` SET bAnnule = 0 WHERE id_test =:id";

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id', $id);
		$bool = $commande->execute();
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function afficher_quest($id_test)
{
	require("modele/connect.php");
	$sql = "SELECT q.*, qc.* FROM question q ,qcm qc, test tst WHERE qc.id_quest = q.id_quest AND qc.id_test = tst.id_test AND tst.id_test =:id_test";
	$tab_quest = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_test', $id_test);
		$bool = $commande->execute();

		if ($bool) {
			$tab_quest = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			//var_dump($tab_quest);
			// die('Arrête toi ici');
		} else {
			die('afficher question ca marche pas');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_quest;
}

function afficher_rep()
{
	require("modele/connect.php");
	$sql = "SELECT rep.id_quest, rep.bvalide, rep.texte_rep, rep.id_rep FROM reponse rep, question qst WHERE qst.id_quest = rep.id_quest";
	$tab_rep = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_quest', $id_quest);
		$bool = $commande->execute();

		if ($bool) {
			$tab_rep = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			// var_dump($tab_rep);
			// die('Arrête toi ici');
		} else {
			die('afficher rep ca marche pô');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_rep;
}



/** NOTE Groupes de l'étudiant *****************/

function groupes_etu($id_etu)
{
	require("modele/connect.php");
	$sql = "SELECT groupe.num_grpe FROM `grpetudiants` ,`groupe` WHERE groupe.id_grpe = grpetudiants.id_grpe AND grpetudiants.id_etu =:id_etu;";
	$tab_etu = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_etu', $id_etu);
		$bool = $commande->execute();

		if ($bool) {
			$tab_etu = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			// var_dump($tab_etu);
			// die('Arrête toi ici');
		} else {
			die('groupe etu ca marche pas');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_etu;
}



/** NOTE Booleans de gestion du qcm  *****************/

function autoriser_qcm($id_qcm)
{
	require("modele/connect.php");
	$sql = "UPDATE `qcm` SET bAutorise = 1 WHERE id_qcm =:id_qcm; UPDATE `qcm` SET bBloque = 0 WHERE id_qcm =:id_qcm;";


	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_qcm', $id_qcm);
		$bool = $commande->execute();

		if (!$bool) {
			die("marche pô, c'est dommaaaaage HAHAHAHAHAHA, autoriser_qcm marche paaaaaaaaa");
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function bloquer_qcm($id_qcm)
{
	require("modele/connect.php");
	$sql = "UPDATE `qcm` SET bAutorise = 0 WHERE id_qcm =:id_qcm; UPDATE `qcm` SET bBloque = 1 WHERE id_qcm =:id_qcm;";

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_qcm', $id_qcm);
		$bool = $commande->execute();
		if (!$bool) {
			die("marche pô, c'est dommaaaaage HAHAHAHAHAHA, bloquer_qcm marche paaaaaaaaa");
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function annuler_qcm($id_qcm, $id_quest)
{
	require("modele/connect.php");
	$sql = "UPDATE `qcm` SET bAnnule = 1 WHERE id_qcm =:id_qcm; DELETE FROM `resultat` WHERE id_quest=:id_quest ";

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_qcm', $id_qcm);
		$commande->bindParam(':id_quest', $id_quest);
		$bool = $commande->execute();
		if (!$bool) {
			die("marche pô, c'est dommaaaaage HAHAHAHAHAHA, annuler_qcm marche paaaaaaaaa");
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function reponse_estValide($id_rep)
{

	return true;
}



/** NOTE Gestion des résultats  *****************/

function reponses_eleve($id_test)
{
	require("modele/connect.php");
	$sql = "SELECT rep.id_rep FROM reponse rep, resultat res WHERE rep.id_rep = res.id_rep AND res.id_test=:id_test";
	$tab_res = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_test', $id_test);
		$bool = $commande->execute();

		if ($bool) {
			$tab_res = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			// var_dump($tab_res);
			// die('Arrête toi ici');
		} else {
			die('erreur select note eleve');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_res;
}

function enregistrer_resultat($id_test, $id_etu, $id_quest, $id_rep)
{
	require("modele/connect.php");
	$dateR = date("Y-m-d");
	$sql = "INSERT INTO `resultat`(id_test, id_etu, id_quest, date_res, id_rep)
			VALUES (:id_test, :id_etu, :id_quest, :dateR, :id_rep) ";
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_test', $id_test);
		$commande->bindParam(':id_quest', $id_quest);
		$commande->bindParam(':id_etu', $id_etu);
		$commande->bindParam(':id_rep', $id_rep);
		$commande->bindParam(':dateR', $dateR);
		$bool = $commande->execute();
		if (!$bool) {
			die();
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function enregistrer_bilan($id_test, $id_etu, $note_test)
{
	require("modele/connect.php");
	$date_bilan = date("Y-m-d");
	$sql = "INSERT INTO `bilan`(id_test, id_etu, note_test, date_bilan)
			VALUES (:id_test, :id_etu, :note_test, :date_bilan)";
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_test', $id_test);
		$commande->bindParam(':id_etu', $id_etu);
		$commande->bindParam(':note_test', $note_test);
		$commande->bindParam(':date_bilan', $date_bilan);
		$bool = $commande->execute();
		if (!$bool) {
			die("requette fausse enregistrer bilan");
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec d'update : " . $e->getMessage() . "\n");
		die();
	}
}

function liste_bilans($id_etu)
{
	require("modele/connect.php");
	$sql = "SELECT b.*, t.titre_test FROM bilan b, test t WHERE b.id_test = t.id_test AND b.id_etu=:id_etu";
	$tab_bilans = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_etu', $id_etu);
		$bool = $commande->execute();
		if ($bool) {
			$tab_bilans = $commande->fetchAll(PDO::FETCH_ASSOC);
			// var_dump($tab_test);
			// die('Arrête toi ici');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_bilans;
}
function creer_question($id_test, $id_theme)
{
	require("modele/connect.php");
	$sql = "INSERT INTO `question`(id_quest, id_theme, titre, texte, bmultiple)
	VALUES (:id_quest, :id_theme, :titre, :texte, :bmultiple) ";
	$tab_newQst = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_quest', $id_quest);
		$commande->bindParam(':id_theme', $id_theme);
		$commande->bindParam(':titre', $titre);
		$commande->bindParam(':texte', $texte);
		$commande->bindParam(':bmultiple', $bmultiple);
		$bool = $commande->execute();
		if ($bool) {
			$tab_newQst = $commande->fetchAll(PDO::FETCH_ASSOC);
			// var_dump($tab_test);
			// die('Arrête toi ici');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de creation de question : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_newQst;
}

function creer_test()
{
	require("modele/connect.php");
	$sql = "INSERT INTO `test`(id_test, id_prof, num_grpe, titre_test, date_test, bActif)
	VALUES (:id_test, :id_prof, :num_grpe, :titre_test, :date_test, :bActif) ";
	$tab_newTest = array();

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':id_test', $id_test);
		$commande->bindParam(':id_prof', $id_prof);
		$commande->bindParam(':num_grpe', $numgrpe);
		$commande->bindParam(':titre_test', $titre_test);
		$commande->bindParam(':date_test', $date_test);
		$commande->bindParam(':bActif', $bActif);
		$bool = $commande->execute();
		if ($bool) {
			$tab_newTest = $commande->fetchAll(PDO::FETCH_ASSOC);
			// var_dump($tab_test);
			// die('Arrête toi ici');
		}
	} catch (PDOException $e) {
		echo utf8_encode("Echec de creation de question : " . $e->getMessage() . "\n");
		die();
	}
	return $tab_newTest;
}