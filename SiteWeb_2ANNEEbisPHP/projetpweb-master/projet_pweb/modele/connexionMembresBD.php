<?php 
	

function connexion_etu_BD($login,$password){ 
	require ("modele/connect.php") ; 
	$sql= "SELECT * FROM `etudiant` WHERE login_etu=:login AND pass_etu=:password";
	$tab_etu= array(); 
	
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':login', $login);
		$commande->bindParam(':password', $password);
		$bool = $commande->execute();
		if ($bool) {
			$tab_etu = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			//var_dump($tab_etu); die ('arret ici');
			}
		}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	
	if (count($tab_etu)== 0) return null; 
	else return $tab_etu[0];
}

function connexion_prof_BD($login,$password){ 
	require ("modele/connect.php") ; 
	$sql= "SELECT * FROM `professeur` WHERE login_prof=:login AND pass_prof=:password";
	$tab_prof= array(); 
	
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':login', $login);
		$commande->bindParam(':password', $password);
		$bool = $commande->execute();
		if ($bool) {
			$tab_prof = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			//var_dump($tab_prof); die ('arret ici ici');

			}
		}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	
	if (count($tab_prof)== 0) return null; 
	else return $tab_prof[0];
}

?>
