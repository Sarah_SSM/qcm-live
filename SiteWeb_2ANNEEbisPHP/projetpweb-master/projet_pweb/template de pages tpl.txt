<html>

<head>
    <?php
    require("vue/head.tpl");
    ?>
</head>

<body>
    <?php
    require("vue/baniere.tpl");
    ?>
    <div class="container-fluid">
        <div class="row">
            <?php
            require("vue/etudiant/etuMenu.tpl"); // si prof remplacer par professeur/profMenu
            ?>
            <div class="col-md-10 col-lg-10 col-sm-12">
                <div class="elements">
                
                </div>
            </div>
        </div>
    </div>
</body>

</html>