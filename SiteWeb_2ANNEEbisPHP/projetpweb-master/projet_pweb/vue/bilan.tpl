<html>

<head>
    <?php
    require("vue/head.tpl");
    ?>
</head>

<body>


    <?php
    require("vue/baniere.tpl");
    ?>
    <div class="container-fluid">
        <div class="row">
            <?php
            require("vue/etudiant/etuMenu.tpl");
            ?>
            <div class="col-md-10 col-lg-10 col-sm-12">
                <div class="elements">
                    <!-- <div class="card" style='width=30%'>
                        <div class="card-body">
                            <h5 class="card-title"><?php echo "<h5> Bilan du test " . $test . "</h5>"; ?></h5>
                            <p class="card-text"> -->
                                <?php

                                $reponsesvalides = 0;
                                $reponsesValidesEtu = 0;
                                foreach ($tabQuest as $quest) {
                                    // if (($quest['bAutorise'] == 1 || $quest['bBloque'] == 0) && $quest['bAnnule'] == 0) {
                                    echo ('</br><h6>' . $quest['titre'] . ' : ' . $quest['texte'] . '</h6></br>');
                                    foreach ($tabRep as $rep) {
                                        if ($rep['id_quest'] == $quest['id_quest']) {
                                            $color = "black";
                                            $coche = "[-]";
                                            foreach ($tabRes as $repCochee) {
                                                $coche = "[-]";
                                                if ($rep['id_rep'] == $repCochee['id_rep']) {
                                                    $coche = "[X]";
                                                    if ($rep['bvalide'] == 1) {
                                                        $reponsesValidesEtu = $reponsesValidesEtu + 1;
                                                        $coche = "<img src='vue/images/brep.jpg' style='width:21px'></img>";
                                                    } else {
                                                        $reponsesValidesEtu = $reponsesValidesEtu - 0.5;
                                                        $coche =  "<img src='vue/images/croix1.webp' style='width:19px'></img>";
                                                    }
                                                    break;
                                                }
                                            }
                                            if ($rep['bvalide'] == 1) {
                                                $reponsesvalides = $reponsesvalides + 1;
                                                $color = "green";
                                            } else if ($rep['bvalide'] == 0) {
                                                $coche = "<del style ='color:red;'>" . $coche . "</del>";
                                            }

                                            echo ('<div style="color:' . $color . ';">' . $coche . ' ' . $rep['texte_rep'] . '</div>');
                                        }
                                    }
                                }
                                // }
                                $note = $reponsesValidesEtu . " / " . $reponsesvalides;
                                echo ("</br><h4> Note : " . $note . "</h4>");
                                if (isset($_GET['finTest'])) {
                                    enregistrer_bilan($test, $_SESSION['etu']['id_etu'], $note);
                                }
                                ?></p><br>

                        <!-- </div>
                    </div> -->


                </div>
            </div>
        </div>
    </div>

</body>


</html>