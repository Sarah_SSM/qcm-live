<html>
<div>
    <h2> Tests actifs pour vos groupes : </h2>
    <?php
    foreach ($tabTests as $test) {
        $dejaFait = false;
        foreach ($grpsEtu as $groupe) {
            if ($test['bActif'] == 1 && $groupe['num_grpe'] == $test['num_grpe']) {
                foreach ($tabBilans as $bilan) {
                    if ($bilan['id_test'] == $test['id_test']) {
                        $dejaFait = true;
                        break;
                    }
                }
                if ($dejaFait) {
                    break;
                }
                echo ("<div> --> <a href='index.php?controle=etudiant&action=lancer_test&test=" . $test['id_test'] . "&iQuest=1'>" .
                    $test['titre_test'] . "</a> </div>");
            }
            if ($dejaFait) {
                break;
            }
        }
    }
    ?>
</div>

</html>