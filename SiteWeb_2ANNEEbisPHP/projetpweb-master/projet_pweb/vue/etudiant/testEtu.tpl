<html>

<head>
    <?php
    require("vue/head.tpl");
    ?>
</head>

<body>
    <?php
    require("vue/baniere.tpl");
    ?>
    <div class="container-fluid">
        <div class="row">
            <?php
            require("vue/etudiant/etuMenu.tpl");
            ?>
            <div class="col-md-10 col-lg-10 col-sm-12">
                <div class="elements">

                    <!-- tester si l'utilisateur est connecté -->
                    <?php
                    if ($_SESSION['etu'] != null) {
                        $user = $_SESSION['etu']['prenom'];
                        // afficher un message
                        echo "Session de $user";
                    }
                    ?>
                    <div>
                        <?php
                        if ($_GET !== null) {
                            require_once("modele/sessionBD.php");
                            $test_en_cours = $_GET['test'];
                            echo "test $test_en_cours </br>";
                            $tabquest = afficher_quest($test_en_cours);
                            $tabrep = afficher_rep();
                            $_SESSION['etu']['indiceQuest'] = $_GET['iQuest'];

                            if (isset($tabquest[$_SESSION['etu']['indiceQuest'] - 1])) {
                                $quest = $tabquest[$_SESSION['etu']['indiceQuest'] - 1];
                                if (($quest['bAutorise'] == 1 || $quest['bBloque'] == 0) && $quest['bAnnule'] == 0) {
                                    $_SESSION['etu']['id_quest'] = $quest['id_quest'];
                                    $_SESSION['etu']['id_test'] = $test_en_cours;
                                    $tabquest = afficher_quest($test_en_cours);
                                    require("vue/etudiant/QuestEtu.tpl");
                                } else {
                                    // $_SESSION['etu']['questionscachees'][$quest['id_quest']];
                                    $_SESSION['etu']['indiceQuest'] = $_SESSION['etu']['indiceQuest'] + 1;
                                    header ("Location: index.php?controle=etudiant&action=lancer_test&test=".
                                    $test_en_cours."&iQuest=".$_SESSION['etu']['indiceQuest']."");
                                }
                            } else {
                                // echo "</br> Fin du test </br> 
                                //     <a href='index.php?controle=etudiant&action=accueil'> Retour à l'accueil </a></br>
                                //     <a href='index.php?controle=gestionReponses&action=afficher_bilan_eleve&idTest=".$test_en_cours."'> Voir les résultats </a>";
                                header("Location: index.php?controle=gestionReponses&action=afficher_bilan_eleve&idTest=".$test_en_cours."&finTest=yes");
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

</html>