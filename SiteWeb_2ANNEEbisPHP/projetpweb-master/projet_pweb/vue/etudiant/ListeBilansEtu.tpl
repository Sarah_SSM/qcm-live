<html>

<head>
    <?php
    require("vue/head.tpl");
    ?>
</head>

<body>
    <?php
    require("vue/baniere.tpl");
    ?>
    <div class="container-fluid">
        <div class="row">
            <?php
            require("vue/etudiant/etuMenu.tpl"); 
            ?>
            <div class="col-md-10 col-lg-10 col-sm-12">
                <div class="elements">
                    
                <?php
                echo ("
                
                
<h3> Mes bilans : </h3></br>");
                $bilans = liste_bilans($_SESSION['etu']['id_etu']);
                foreach ($bilans as $bilan){
                    echo(" - <a href='index.php?controle=gestionReponses&action=afficher_bilan_eleve&idTest="
                    .$bilan['id_test']."'>".$bilan['titre_test']."</a></br>");
                }
                
                ?>
                </div>
            </div>
        </div>
    </div>
    
</body>

</html>