<html>

<head>
    <?php
    require("vue/head.tpl");
    ?>
</head>

<body>
    <?php
    require("vue/baniere.tpl");
    ?>
    <div class="container-fluid">
        <div class="row">
            <?php
            require("vue/professeur/profMenu.tpl");
            ?>
            <div class="elements">
                <div class="col-md-10">
                    <div>
                        <?php
                        echo ("<h2> Page du test n° " . $idTest . " </h2>");
                        ?>
                    </div>
                    <div>
                        <?php
                        $tabQuest = afficher_quest($idTest);
                        $tabRep = afficher_rep();
                        require("vue/professeur/listeQuest.tpl");
                        ?>
                    </div>
                    <div>
                        </br>
                        <hr />
                        <?php
                        echo ("<div> <a href='index.php?controle=professeur&action=accueilSimple'> Retourner au menu </a> </div>");
                        echo ("<div> <a href='index.php?controle=professeur&action=desac_test&test=" . $idTest . "'> desactiver </a> </div>");
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>