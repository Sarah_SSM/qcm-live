<html>
    <div>
        <h2> Questions : </h2>
        <?php
        $cpt = 0;
            foreach($tabQuest as $quest) {
                
                $autorise = ($quest['bAutorise'] == 0 & $quest['bBloque'] == 1) ? 
                    "<a href='index.php?controle=professeur&action=autoriser_quest&test=".$idTest."&quest=".$quest['id_qcm']."'> Autoriser </a>" 
                    : "<a href='index.php?controle=professeur&action=bloquer_quest&test=".$idTest."&quest=".$quest['id_qcm']."'> Bloquer </a>";
                
                $annulay = ($quest['bAnnule'] == 0) ?
                    "<a href='index.php?controle=professeur&action=annuler_quest&test=".$idTest."&quest=".$quest['id_quest']."&qcm=".$quest['id_qcm']."'> Annuler </a>" 
                    : "";

                echo($quest['titre']. " ( ". $quest['texte'] . " ) ".$autorise."  ".$annulay. "</br>");
                echo("<i> -- > Réponses </i></br>");

                foreach($tabRep as $rep){
                    $valide = ($rep['bvalide'] == 0) ? "": "(Bonne réponse)";
                    if($quest['id_quest'] == $rep['id_quest']) {
                        echo(" -------- ".$rep['texte_rep']. "  <i>".$valide."</i> </br>");
                    }
                }

                echo("</br>");
                $cpt = $cpt +1;
                
            }
            if ($cpt == 0){
                echo("ce test ne contient pas encore de questions");
            }
        ?>
    </div>
</html>
