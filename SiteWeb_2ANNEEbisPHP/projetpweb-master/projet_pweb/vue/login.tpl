<html>

<head>
  <meta charset="utf-8">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!-- Include the above in your HEAD tag -->

  <link rel="stylesheet" href="vue/styles/styleLogin.css" media="screen" type="text/css" />
  <link rel="stylesheet" href="vue/styles/styleAnim.css" media="screen" type="text/css" />
</head>

<body>

  <div class="main">


    <div class="container">
      <div class="svg-wrapper">
        <svg height="60" width="320" xmlns="http://www.w3.org/2000/svg">
          <rect class="shape" height="60" width="320" />
          <div class="text">CONNEXION</div>
        </svg>
      </div>

      <center>
        <div class="middle">
          <div id="login">

            <form action="index.php?controle=ident&action=ident" method="POST">

              <fieldset class="clearfix">

                <p><span class="fa fa-user"></span><input type="text" name="login" Placeholder="Nom d'utilisateur" required></p> <!-- JS because of IE support; better: placeholder="Username" -->
                <p><span class="fa fa-lock"></span><input type="password" name="password" Placeholder="Mot de passe" required></p> <!-- JS because of IE support; better: placeholder="Password" -->

                <div>
                  <span style="width:48%; text-align:left;  display: inline-block;"><a class="small-text" href="#">Mot de passe oublié ?</a></span>
                  <span style="width:50%; text-align:right;  display: inline-block;">
                    <input type="submit" id="submit" value="Valider">
                    <?php
                    if (isset($_GET['erreur'])) {
                      $err = $_GET['erreur'];
                      if ($err == 1 || $err == 2) {
                        echo "<p style='color:red'></p>";
                      }
                    }
                    ?></span>
                </div>

              </fieldset>
              <div class="clearfix"></div>
            </form>

            <div class="clearfix"></div>

          </div> <!-- end login -->
          <div class="logo"><img src="vue/images/niark.png"></img>
            <div class="clearfix"></div>
          </div>

        </div>
      </center>
    </div>
    <div id=msgerreur>
      <?php echo $msg;
      ?>
    </div>
  </div>

  </div>
  </div>
  </div>

</body>

</html>