<?php


function accueil() {
	$idEtu = $_SESSION['etu']['id_etu'];
	require ("modele/sessionBD.php");
	$tabTests = afficher_test();
	$tabBilans = liste_bilans($idEtu);
	$grpsEtu = groupes_etu($idEtu);
	require ("./vue/etudiant/AccueilEtu.tpl");
}

function lancer_test(){
	require ("vue/etudiant/testEtu.tpl");
}

function deconnexion(){
	session_destroy();
	header ("Location: index.php");
}

function bilan() {
	require ("modele/sessionBD.php");
	require ("vue/etudiant/ListeBilansEtu.tpl");
}
?>