<?php


function accueil() {
	require ("modele/sessionBD.php");
	$tabTests = afficher_test();
	require ("./vue/professeur/AccueilProf.tpl");
}
function accueilSimple() {
	require ("modele/sessionBD.php");
	$tabTests = afficher_test();
	require ("./vue/professeur/AccueilProfSmp.tpl");
}


function lancer_test(){
	$idTest = $_GET['test'];

	//set le test à "actif" dans la base de donnée
	require("modele/sessionBD.php");
	activer_test($idTest);

	require("vue/professeur/test.tpl");
}

function desac_test(){
	$idTest = $_GET['test'];
	require ("modele/sessionBD.php");
	desactiver_test($idTest);
	require ("./vue/professeur/AccueilProfSmp.tpl");
}

/* function affich_quest(){
	$idTest = $_GET['test'];
	require ("modele/sessionBD.php");
	afficher_quest($idTest);
	require("vue/professeur/test.tpl");
} */

function autoriser_quest(){
	$idTest = $_GET['test'];
	$idQcm = $_GET['quest'];
	require ("modele/sessionBD.php");
	autoriser_qcm($idQcm);
	require ("vue/professeur/test.tpl");
}

function bloquer_quest(){
	$idTest = $_GET['test'];
	$idQcm = $_GET['quest'];
	require ("modele/sessionBD.php");
	bloquer_qcm($idQcm);
	require ("vue/professeur/test.tpl");
}

function annuler_quest(){
	$idTest = $_GET['test'];
	$idQcm = $_GET['qcm'];
	$idQuest = $_GET['quest'];
	require ("modele/sessionBD.php");
	annuler_qcm($idQcm,$idQuest);
	require ("vue/professeur/test.tpl");
}


function deconnexion(){
	session_destroy();
	header ("Location: index.php");
}
?>