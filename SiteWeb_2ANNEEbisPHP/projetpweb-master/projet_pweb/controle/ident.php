<?php
    function ident(){
        if (isset ( $_POST ['login'] ) && isset ( $_POST ['password'] )) {
            $login = $_POST ['login'];
            $password = $_POST ['password'];

            require("modele/connexionMembresBD.php");
            if (($infos_etu = connexion_etu_BD($login,$password)) != null){
                //session_start();
                $_SESSION['etu'] = $infos_etu;
                $url = "index.php?controle=etudiant&action=accueil";
			    header ("Location:" .$url) ;
            
            } else if (($infos_prof = connexion_prof_BD($login,$password)) != null) {
                //session_start();
                $_SESSION['prof'] = $infos_prof;
                $url = "index.php?controle=professeur&action=accueil";
			    header ("Location:" .$url) ;

            } else {
                $msg ="Utilisateur inconnu";
                require("vue/login.tpl");
            }

        }else {
            $msg='';
            require("vue/login.tpl");
        }
    }
?>